from sqlalchemy import create_engine, Column, Integer, String, func
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from config import *
from DBClasses import User, Options
import telebot
from telebot import types
import json


class CofeeBot():
    bot = telebot.TeleBot(BotToken)
    def  __init__(self) -> None:
        self.Base = declarative_base()
        self.engine = create_engine('sqlite:///database.db', echo=True)
        self.Session = sessionmaker(bind=self.engine)
        session = self.Session()
        User.__table__.create(bind=self.engine, checkfirst=True)
        Options.__table__.create(bind=self.engine, checkfirst=True)
        self.Base.metadata.create_all(self.engine)
        #new_user = User(name='John Doe')
        #session.add(new_user)
        session.commit()
        session.close()
        

    def  Start(self):
        self.bot.polling(non_stop=True, interval=0)


CB = CofeeBot()


@CB.bot.message_handler(commands=['ping'])
def pong(mes):
    CB.bot.send_message(mes.chat.id, "pong")


@CB.bot.message_handler(commands=['cup'])
def cup(mes):
    cbdata =  {
        "user":mes.from_user.id,
        "type":"CUP",
        "buttonnum":1
        }
    to_markup = types.InlineKeyboardMarkup(row_width=2)
    item1 = types.InlineKeyboardButton('☕️', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 2
    item2 = types.InlineKeyboardButton('☕️☕️', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 3
    item3 = types.InlineKeyboardButton('Отмена', callback_data=json.dumps(cbdata))
    to_markup.add(item1, item2)
    to_markup.add(item3)
    CB.bot.reply_to(mes, 'Что пьем:', reply_markup=to_markup)
    

@CB.bot.message_handler(commands=['end_period'])
def end_period(mes):
    if not mes.from_user.id in AdminsId:
        CB.bot.send_message(mes.chat.id, "Ты не админ")
        return
    session = CB.Session()
    price =  0
    option  =  session.query(Options).filter_by(TgChatId=str(mes.chat.id)).first()
    if option:
        price = option.CoffeCupPrice
    else:
        new_option = Options(TgChatId = str(mes.chat.id))
        session.add(new_option)
        price = 20
    users = session.query(User).filter_by(TgChatId=str(mes.chat.id)).order_by(User.CoffeCupCount.desc())
    if users:
        res = ""
        for user in users:
            res += f"{user.name} : {user.CoffeCupCount} чашек : {user.CoffeCupCount*price} р.\n"
            user.CoffeCupCount  = 0
        CB.bot.send_message(mes.chat.id, res)
    else:
        CB.bot.send_message(mes.chat.id, "Тут не кто еще не пил кофе")
    session.commit()
    session.close()


@CB.bot.message_handler(commands=['set_price'])
def set_price(mes):
    if not mes.from_user.id in AdminsId:
        CB.bot.send_message(mes.chat.id, "Ты не админ")
        return
    session = CB.Session()
    option  =  session.query(Options).filter_by(TgChatId=str(mes.chat.id)).first()
    if option:
        try:
            price = mes.text.split(' ')[1]
            option.CoffeCupPrice =  mes.text.split(' ')[1]
            CB.bot.send_message(mes.chat.id, price)
        except:
            CB.bot.send_message(mes.chat.id, "Что то пошло не так попробуйте еще раз")
    else:
        new_option = Options(TgChatId = str(mes.chat.id))
        session.add(new_option)
    session.commit()
    session.close()


@CB.bot.message_handler(commands=['statistic'])
def statistic(mes):
    cbdata =  {
        "user":mes.from_user.id,
        "type":"STAT",
        "buttonnum":1
        }
    to_markup = types.InlineKeyboardMarkup(row_width=2)
    item1 = types.InlineKeyboardButton('☕️ Личная', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 2
    item2 = types.InlineKeyboardButton('☕️ Общая', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 3
    item3 = types.InlineKeyboardButton('ТО Личная', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 4
    item4 = types.InlineKeyboardButton('ТО общая', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 5
    item5 = types.InlineKeyboardButton('Отмена', callback_data=json.dumps(cbdata))
    to_markup.add(item1, item2)
    to_markup.add(item3, item4)
    to_markup.add(item5)
    CB.bot.reply_to(mes, 'Что хотим узнать:', reply_markup=to_markup)


@CB.bot.message_handler(commands=['about'])
def About(mes):
    CB.bot.reply_to(mes, "Бот для учета выпитого кофе\nРазработал криворукий дебил\n Посмотреть исходдный код можно по ссылке: https://gitlab.com/Computer-mage/coffeebottelegram")

@CB.bot.message_handler(commands=['to'])
def tocomm(mes):
    cbdata =  {
        "user":mes.from_user.id,
        "type":"TO",
        "buttonnum":1
        }
    to_markup = types.InlineKeyboardMarkup(row_width=2)
    item1 = types.InlineKeyboardButton('Выкинул жмых', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 2
    item2 = types.InlineKeyboardButton('Вымыл контейнер от молока', callback_data=json.dumps(cbdata))
    cbdata["buttonnum"] = 3
    item3 = types.InlineKeyboardButton('Отмена', callback_data=json.dumps(cbdata))
    to_markup.add(item1, item2)
    to_markup.add(item3)
    CB.bot.reply_to(mes, 'Выберете тип ТО:', reply_markup=to_markup)


@CB.bot.callback_query_handler(func=lambda call: True)
def handle_callback_query(call):
    chat_id = call.message.chat.id
    message_id = call.message.message_id
    user_id = call.from_user.id
    cbdata  =  json.loads(call.data)
    if(isinstance(cbdata, dict)):
        if cbdata["type"] == "TO":
            if cbdata["buttonnum"] == 1:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                    if user:
                        user.ToJmihCount += 1
                    else:
                        new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username), ToJmihCount=1)
                        session.add(new_user)
                    session.commit()
                    session.close()
                    CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Спасибо что выкинул жмых!')
                    CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                    CB.bot.answer_callback_query(call.id, 'Выполнено')
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 2:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                    if user:
                        user.ToKonteinerCount += 1
                    else:
                        new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username), ToKonteinerCount=1)
                        session.add(new_user)
                    session.commit()
                    session.close()
                    CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text='Спасибо что помыл контейнер!')
                    CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                    CB.bot.answer_callback_query(call.id, 'Выполнено')
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 3: 
                if   user_id == cbdata["user"]:
                    CB.bot.delete_message(chat_id, message_id)
                    CB.bot.delete_message(chat_id, call.message.reply_to_message.message_id)
                    CB.bot.answer_callback_query(call.id, 'Выполнено')
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
        elif  cbdata["type"] == "CUP":
            if cbdata["buttonnum"] == 1:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                    if user:
                        user.CoffeCupCount += 1
                        session.commit()
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"Чашка учтена!\nВы уже выпили {user.CoffeCupCount} чашек!")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    else:
                        new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username), CoffeCupCount = 1)
                        session.add(new_user)
                        session.commit()
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text="Чашка учтена!\nВы уже выпили 1 чашек!")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    session.close()
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 2:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                    if user:
                        user.CoffeCupCount += 2
                        session.commit()
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"Чашка учтена!\nВы уже выпили {user.CoffeCupCount} чашек!")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    else:
                        new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username), CoffeCupCount = 2)
                        session.add(new_user)
                        session.commit()
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text="Чашка учтена!\nВы уже выпили 2 чашек!")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    session.close()
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 3:
                if   user_id == cbdata["user"]:
                    CB.bot.delete_message(chat_id, message_id)
                    CB.bot.delete_message(chat_id, call.message.reply_to_message.message_id)
                    CB.bot.answer_callback_query(call.id, 'Выполнено')
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
        elif cbdata["type"] == "STAT":
            if cbdata["buttonnum"] == 1:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                    if user:
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"{user.name}, ты выпил {user.CoffeCupCount} чашек")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    else:
                        new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username))
                        session.add(new_user)
                        session.commit()
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"{call.message.reply_to_message.from_user.first_name}, ты выпил 0 чашек")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    session.close()
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 2:
                if   user_id == cbdata["user"]:
                        session = CB.Session()
                        CupCount = session.query(func.sum(User.CoffeCupCount)).filter(User.TgChatId == str(chat_id)).scalar()
                        CupUser = 0
                        user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                        if user:
                            CupUser = user.CoffeCupCount
                        else:
                            new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username))
                            session.add(new_user)
                            session.commit()
                        users = session.query(User).filter_by(TgChatId=str(chat_id)).order_by(User.CoffeCupCount.desc()).limit(3)
                        top = "Ударники кофе потребления:\n"
                        for u in users:
                            top += f"\t\t{u.name} - {u.CoffeCupCount} чашек\n"
                        pr = CupUser/(CupCount/100)
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"Ты выпил {CupUser} это {round(pr, 3)}% от общего количества!\n\n{top}")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                        session.close()
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 3:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    user = session.query(User).filter_by(TgId = str(user_id), TgChatId=str(chat_id)).first()
                    if user:
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"{user.name}, ты {user.ToJmihCount} раз выкинул жмых и {user.ToKonteinerCount} раз помыл контейнер")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    else:
                        new_user = User(name=str(call.message.reply_to_message.from_user.first_name), TgId = str(user_id), TgChatId=str(chat_id), TgNikname=str(call.message.reply_to_message.from_user.username))
                        session.add(new_user)
                        session.commit()
                        CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                              text=f"{call.message.reply_to_message.from_user.first_name}, ты 0 раз выкинул жмых и 0 раз помыл контейнер")
                        CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=None)
                        CB.bot.answer_callback_query(call.id, 'Выполнено')
                    session.close()
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 4:
                if   user_id == cbdata["user"]:
                    session = CB.Session()
                    users = session.query(User).filter_by(TgChatId=str(chat_id)).order_by(User.ToJmihCount.desc()).limit(3)
                    top = "Больше всего раз выкинули жмых:\n"
                    for u in users:
                        top += f"\t\t{u.name} - {u.ToJmihCount} раз\n"
                    users = session.query(User).filter_by(TgChatId=str(chat_id)).order_by(User.ToKonteinerCount.desc()).limit(3)
                    top += "\nБольше всего раз вымыли контейнер:\n"
                    for u in users:
                        top += f"\t\t{u.name} - {u.ToKonteinerCount} раз\n"
                    CB.bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                        text=top)
                    CB.bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id, reply_markup=None)
                    CB.bot.answer_callback_query(call.id, 'Выполнено')
                    session.close()
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
            elif cbdata["buttonnum"] == 5:
                if   user_id == cbdata["user"]:
                    CB.bot.delete_message(chat_id, message_id)
                    CB.bot.delete_message(chat_id, call.message.reply_to_message.message_id)
                    CB.bot.answer_callback_query(call.id, 'Выполнено')
                else:
                    CB.bot.answer_callback_query(call.id, 'Вы не можете выполнить это действие.')
    else:
        CB.bot.answer_callback_query(call.id, 'Команда не распознана')
    

if __name__ == "__main__":
    CB.Start()