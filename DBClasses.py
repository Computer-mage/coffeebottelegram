from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    TgId = Column(String)
    TgChatId = Column(String)
    TgNikname = Column(String)
    CoffeCupCount = Column(Integer, default=0)
    ToJmihCount = Column(Integer, default=0)
    ToKonteinerCount = Column(Integer, default=0)


class Options(Base):
    __tablename__ = 'options'
    id = Column(Integer, primary_key=True)
    CoffeCupPrice = Column(Integer, default=20)
    AddToMes = Column(String, default="")
    TgChatId = Column(String)